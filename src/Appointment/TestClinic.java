package Appointment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class TestClinic 
{
	public static void main(String[] args) throws Throwable 
	{
		Scanner s=new Scanner(System.in);
		System.out.println("enter no of doctors");
		int num=s.nextInt();
		Doctor[] doc=new Doctor[num];
		
		for(int i=0;i<num;i++)
		{
			System.out.println( "enter doctor"+(i+1)+"details");
			System.out.println("-------********---------");
			System.out.println("enter doc id "); String docID=s.next();
			System.out.println("enter docname "); String docName=s.next();
			doc[i]=new Doctor();
			doc[i].setDocID(docID);
			doc[i].setDocName(docName);
		}
		
		
		int count[]=new int[num];
		
		for(int i=0;i<num;i++)
		{
		 System.out.println("enter the no of appointments for doctor"+" "+(i+1)+"");	
		 int n=s.nextInt();
		 count[i]=n;
		     for(int j=0;j<n;j++)
		     {
		    	 Appointment ap=new Appointment();
		    	 
		    	 
		    	 System.out.println("enter the appointment date in (dd/mm/yyyy)format");
		    	 DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
		    	 ap.setAppDate(df.parse(s.next()));
		    	// doc[i].addAppointment(ap);	
		    	 
		    	 System.out.println("enter no of patients  that should be greater than 1  and less than 15");
		    	 int npat=s.nextInt();
		    	 ap.setNoOfPatients(npat);	
		    	 doc[i].addAppointment(ap);
		     }
		 
		 
		 
		}
		
		for(int i=0;i<num;i++)
		{
			System.out.println("the count is "+count[i]);
			
		}
		
		 int c=0;
		 System.out.println("enter the doctor id  to see appointment details for that particular doctor");
		  String did=s.next();
		 for(int i=0;i<num;i++)
		 {
			 if(did.equals(doc[i].getDocID())) 
			 {
			     doc[i].printAppointment();
			     c++;
			 }
		 }
		 if(c==0)
		 {
			 System.out.println("doctor id doesnot exist");
		 }
		 
		
		
		
		
	}

}
