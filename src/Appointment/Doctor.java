package Appointment;

import java.util.ArrayList;
import java.util.Iterator;

public class Doctor 
{
	private String docID;
	private  String docName;
	private   ArrayList<Appointment> apList =new ArrayList<Appointment>();
	
	public void  addAppointment(Appointment ap)
	{
		apList.add(ap);
		
	}
	
	public void printAppointment()
	{
		System.out.println("*************Appointment details are *****************");
		Iterator<Appointment> itr=apList.iterator();
		while(itr.hasNext())
		{
		 Appointment ap1=(Appointment)itr.next();
	 
		 
		 System.out.println("Appointment date is \t"+ap1.getAppDate());
		 System.out.println("no of patients are \t"+ap1.getNoOfPatients());
		 break;
		 
		}
	}

	public String getDocID() {
		return docID;
	}

	public void setDocID(String docID) {
		this.docID = docID;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}
	
	
	 

}
